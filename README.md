# v4l2camreader

A v42l camera capture implementation in C/C++, largely based on the work of Simen Haugo (https://lightbits.github.io/v4l2_real_time/).