#!/bin/bash

num_frames=0
decompress_jpg=0
stream_video=0
print_timestamps=1
write_to_file=1
camera_name=/dev/video0
camera_width=1920
camera_height=1080
camera_buffers=30

########################################
# PRIOR TO COMPILING
########################################
# check if required packages are present
LIBV4L_EXISTS=$(dpkg -l | grep libv4l-dev | wc -l)
UTILV4L_EXISTS=$(dpkg -l | grep v4l-utils | wc -l)

if [ $LIBV4L_EXISTS -eq 0 ]; then
	sudo apt-get update
	sudo apt-get install libv4l-dev
else
    echo "The libv4l-dev package is already installed. Skipping installation."
fi

if [ $UTILV4L_EXISTS -eq 0 ]; then
	sudo apt-get update
	sudo apt-get install v4l-utils
else
    echo "The v4l-utils package is already installed. Skipping installation."
fi

# ELP FISHEYE CAMERA CONTROLS
# (v4l2-ctl -d/dev/video0 -L)
powerline=0     # (0=off, 1 = 50Hz, 2 = 60Hz)
whitebalance=1  # (0=off, 1=on)
wb_temp=4600    # (min=2800 max=6500 step=10 default=4600)
sharpness=2     # (min=0    max=7   step=1 default=2)
brightness=0    # (min=-64  max=64  step=1 default=0)
contrast=32     # (min=0    max=95  step=1 default=32)
saturation=60   # (min=0    max=100 step=1 default=55)
hue=0           # (min=-2000  max=2000  step=1 default=0)
gamma=165        # (min=100   max=300 step=1 default=165)
gain=0          # (min=0    max=100 step=1 default=0)
exposure=166     # (min=50    max=10000 step=1 default=166)

#
#
#

CAMERA_CONNECTED=$(ls /dev | grep video | wc -l)
if [ $CAMERA_CONNECTED -ne 0 ]
then
    echo "Turning exposure auto mode to aperture priority"
        v4l2-ctl -d $camera_name -c exposure_auto=3
    #echo "Setting exposure to manual"
    #    v4l2-ctl -d $camera_name -c exposure_auto=1
    #echo "Setting exposure time to $exposure"
    #    v4l2-ctl -d $camera_name -c exposure_absolute=$exposure
    # echo "Setting sharpness to $sharpness"
    #     v4l2-ctl -d $camera_name -c sharpness=$sharpness
    # echo "Setting brightness to $brightness"
    #     v4l2-ctl -d $camera_name -c brightness=$brightness
    # echo "Setting contrast to $contrast"
    #     v4l2-ctl -d $camera_name -c contrast=$contrast
    # echo "Setting saturation to $saturation"
    #     v4l2-ctl -d $camera_name -c saturation=$saturation
    # echo "Setting hue to $hue"
    #     v4l2-ctl -d $camera_name -c hue=$hue
    # echo "Setting gamma to $gamma"
    #     v4l2-ctl -d $camera_name -c gamma=$gamma
    # echo "Setting power line frequency to $powerline"
    #     v4l2-ctl -d $camera_name -c power_line_frequency=$powerline
     echo "Setting automatic white balance to $whitebalance"
         v4l2-ctl -d $camera_name -c white_balance_temperature_auto=$whitebalance
else
    echo "Camera not connected to the system. Skipping settings configuration!"
fi

# If you wish to compile and run w/o cmake
#
#DEFINES="-DCAMERA_NAME=\"$camera_name\"
#         -DCAMERA_WIDTH=$camera_width
#         -DCAMERA_HEIGHT=$camera_height
#         -DCAMERA_BUFFERS=$camera_buffers
#         -DNUM_FRAMES=$num_frames
#         -DDECOMPRESS_JPG=$decompress_jpg
#         -DSTREAM_VIDEO=$stream_video
#         -DPRINT_TIMESTAMPS=$print_timestamps
#         -DWRITE_TO_FILE=$write_to_file"
#g++ $DEFINES test_usbcam.cpp -o app -lv4l2 -lturbojpeg && ./app


mkdir -p build
cd build
cmake ..
make
echo "Copying binary to ~/bin/ ..."
cp v4l2camreader ~/bin/
echo "Done!"
